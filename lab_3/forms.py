from lab_1.models import Friend

from django import forms


class DateInput(forms.DateInput):
     input_type = "date"

class DateForm(forms.Form):
    dob = forms.DateField(widget=DateInput)

class FriendFrom(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {"dob":DateInput()}


