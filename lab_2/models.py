from django.core.exceptions import TooManyFieldsSent
from django.db import models

# Create your models here.
class Note(models.Model):
    note_from = "from"
    to = models.CharField(max_length=30)
    locals()[note_from] = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=1000)
