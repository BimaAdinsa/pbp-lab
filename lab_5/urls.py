from django.urls import path
from lab_5.views import home,loginView,logoutView,register,interest

urlpatterns = [
    path('', home, name='home'),
    path('login',loginView,name="loginView"),
    path('logout',logoutView,name="logoutView"),
    path('register',register,name='register'),
    path('interest',interest,name='interest'),
]