Apakah perbedaan antara JSON dan XML?
1. JSON memiliki struktur kode yang sederhana dibandingkan XML, Hal ini berdampak pada penggunaan memori kedua kode tersebut, sehingga berpengaruh juga pada kecepatan proses transfer data.
2. Karena memiliki bentuk yang sederhana struktur kode JSON lebih mudah dipahami
3. XML mendukung banyak tipe data seperti bagan, charts, tipe data primitif maupun non-primitif lainnya. Sedangkan JSON hanya mendukung untuk tipe data primitif.
4. Penyimpanan data pada XML disimpan dalam bentuk tree structure, sedangkan JSON disimpan dalam bentuk map dengan pasangan key dan value
5. Pada XML kita dapat melakukan pemrosesan dan pemformatan dokumen dan objek, sedangkan pada JSON tidak

Apakah perbedaan antara HTML dan XML?
1. HTML berfokus pada penyajian data sedangkan XML berfokus pada transfer data
2. HTML case insensitive sehingga tidak peka terhadap huruf besar dan kecil sedangkan XML case sensitive
3. HTML adalah bahasa markup standar sehingga tagnya terbatas sedangkan XML menyediakan sebuah framework untuk menentukan bahasa markup sehingga developer dapat mengembangkan tagnya.
4. Dalam segi tingkat sensitifitas terhadap error XML tidak boleh memiliki error, sedangkan HTML dapat mengabaikan error kecil
6. XML diharuskan menggunakan tag penutup sedangkan HTML tidak