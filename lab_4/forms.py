from lab_2.models import Note

from django import forms

class NoteFrom(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"