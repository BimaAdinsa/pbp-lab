from django.shortcuts import render
from django.http.response import HttpResponse,HttpResponseRedirect
from django.core import serializers
from lab_2.models import Note
from lab_4.forms import NoteFrom

def index(request):
    note = Note.objects.all().values() 
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    form = NoteFrom(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values() 
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)